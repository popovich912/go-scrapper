package utils

import (
	"fmt"
	"math/rand"
	"time"
)

func TimePrepare(t *time.Time, full bool) string {
	if t == nil {
		return "--"
	}
	yyyy, mm, dd := t.Date()
	if full {
		return fmt.Sprintf("%02d.%02d.%04d-%02d:%02d",
			dd, mm, yyyy, t.Hour(), t.Minute())
	}
	return fmt.Sprintf("%02d.%02d.%04d",
		dd, mm, yyyy)
}

func RandomSleep(max int) int {
	rand.Seed(time.Now().UnixNano())
	min := 1
	return rand.Intn(max-min+1) + min
}

func BDateParse(s string) *time.Time {
	if date, err := time.Parse("2. 1. 2006", s); err == nil {
		return &date
	}
	return nil
}
func MDateParse(s string) *time.Time {
	if date, err := time.Parse("02.01.2006", s); err == nil {
		return &date
	}
	return nil
}

package utils

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func StringInArray(s string, arr []string) bool {
	for _, val := range arr {
		if strings.ToLower(strings.TrimSpace(s)) == strings.ToLower(strings.TrimSpace(val)) {
			return true
		}
	}
	return false
}

func URINormalize(s string) string {
	if !strings.HasPrefix(s, "/") {
		return fmt.Sprintf("/%s", s)
	}
	return s
}

func StringNormalize(s string) string {
	return strings.TrimSpace(s)
}

func ArrayToFilename(arr []string) string {
	return strings.Join(arr, "-")
}

func ToFloat64(s string) float64 {
	if f, err := strconv.ParseFloat(s, 64); err == nil {
		return f
	}
	return 0
}

func PrepareFloat64(f float64) string {
	if f == 0 {
		return ""
	}
	return fmt.Sprintf("%.2f", f)
}

func MakeRoundAndCovering(s string) (r, c string) {
	splData := strings.Split(s, ",")
	r = strings.TrimSpace(splData[len(splData)-2])
	c = strings.TrimSpace(splData[len(splData)-1])
	return
}

func ReplaceNoDigits(s string) string {
	var nonAlphanumericRegex = regexp.MustCompile(`[^0-9]+`)

	return nonAlphanumericRegex.ReplaceAllString(s, "")
}

func ScoreProcessor(s string) (score, roundScore string, ok bool) {
	ok = strings.Contains(s, "<br/>")
	spl := strings.Split(s, "<br/>")
	// for key, val := range spl {
	// 	fmt.Printf("POS:%d VAL:[%s]\n", key, val)
	// }
	score = spl[0]
	if len(spl) == 2 {
		prepare := spl[1]
		prepare = strings.Replace(prepare, "</sup>", "", -1)
		prepare = strings.Replace(prepare, "<sup>", "^", -1)
		prepare = strings.Replace(prepare, "<span>", "", -1)
		prepare = strings.Replace(prepare, "</span>", "", -1)
		roundScore = prepare
	}
	return
}

package utils

import (
	"fmt"
	"os"
)

func CheckPath(s string) bool {
	err := os.MkdirAll(s, os.ModePerm)
	if err != nil {
		fmt.Printf("[ERR-CD]:%+v\n", err)
	}
	return true
}

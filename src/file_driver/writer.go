package filedriver

import (
	"encoding/csv"
	"fmt"
	"go-scrapper/config"
	"go-scrapper/utils"
	"math/rand"
	"os"
	"reflect"
	"time"

	// "github.com/tealeg/xlsx"
	xlsx "github.com/xuri/excelize/v2"
)

type Legend struct {
	// Company1Palyer#OpeningOdds
	C1P1OO string `json:"%s игрок 1 (opening)" format:"c" multiple:"+"`
	C1P2OO string `json:"%s игрок 2 (opening)" format:"c" multiple:"+"`
	// Company1Palyer#ClosingOdds
	C1P1CO string `json:"%s игрок 1 (closing)" format:"c" multiple:"+"`
	C1P2CO string `json:"%s игрок 2 (closing)" format:"c" multiple:"+"`
}

func getHeaders(legend Legend) (header []string) {
	source, _ := ReadSource()
	for _, val := range source.TargetText {
		e := reflect.ValueOf(&legend).Elem()
		for i := 0; i < e.NumField(); i++ {
			varName := e.Type().Field(i).Tag.Get("json")
			if e.Type().Field(i).Tag.Get("format") == "c" {
				varName = fmt.Sprintf(varName, val)
			}
			header = append(header, varName)
		}
	}
	return header
}

var legend = []string{
	"Матч", "Дата", "Раунд",
	"Покрытие", "Название матча", "Призовой капитал",
	"Пол игроков", "Счёт", "Счёт-раунд",
	"Игрок 1", "Игрок 1 - ДР", "Игрок 1 - UID",
	"Игрок 2", "Игрок 2 - ДР", "Игрок 2 - UID",
}

func WriteCSV(body [][]string, affix string) {
	source, _ := ReadSource()
	legend = append(legend, getHeaders(Legend{})...)
	// t := time.Now()
	f, err := os.Create(
		fmt.Sprintf("%s/%s.csv", config.CONF.Output.CSV,
			fmt.Sprintf("%s_%s", utils.ArrayToFilename(source.TargetText), affix),
		),
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer f.Close()
	w := csv.NewWriter(f)
	defer w.Flush()

	if err := w.Write(legend); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}
	for _, row := range body {
		if err := w.Write(row); err != nil {
			log.Fatalln("error writing record to csv:", err)
		}
	}
}

func WriteXLSX(body [][]string, affix string) {
	source, _ := ReadSource()
	// legend := []string{
	// 	"Матч", "Дата", "Раунд", "Покрытие", "Название матча", "Призовой капитал", "Пол игроков", "Игрок 1", "Игрок 1 - ДР", "Игрок 2", "Игрок 2 - ДР",
	// }
	legend = append(legend, getHeaders(Legend{})...)

	file := xlsx.NewFile()
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println(err)
		}
	}()
	sw, err := file.NewStreamWriter("Sheet1")
	if err != nil {
		fmt.Println(err)
		return
	}

	/* WRITE LEGEND */
	lrow := make([]interface{}, len(legend))
	for colID := 0; colID < len(legend); colID++ {
		lrow[colID] = legend[colID]
	}
	if err := sw.SetRow("A1", lrow); err != nil {
		fmt.Println(err)
	}
	// if err := sw.Flush(); err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
	// if err := file.SaveAs("Book1.xlsx"); err != nil {
	// 	fmt.Println(err)
	// }
	/**/

	for pos, rows := range body {

		row := make([]interface{}, len(rows))
		for colID := 0; colID < len(rows); colID++ {
			row[colID] = rows[colID]
		}
		cell, err := xlsx.CoordinatesToCellName(1, (pos + 2))
		if err != nil {
			fmt.Println(err)
			break
		}
		if err := sw.SetRow(cell, row); err != nil {
			fmt.Println(err)
			break
		}

	}
	if err := sw.Flush(); err != nil {
		fmt.Println(err)
		return
	}
	// t := time.Now()
	if err := file.SaveAs(fmt.Sprintf("%s/%s.xlsx", config.CONF.Output.XLSX,
		fmt.Sprintf("%s_%s", utils.ArrayToFilename(source.TargetText), affix),
	)); err != nil {
		fmt.Println(err)
	}
}

func WriteXLSXTest() {
	// legend := []string{
	// 	"Матч", "Дата", "Раунд", "Покрытие", "Название матча", "Призовой капитал", "Пол игроков", "Игрок 1", "Игрок 1 - ДР", "Игрок 2", "Игрок 2 - ДР",
	// }
	legend = append(legend, getHeaders(Legend{})...)

	file := xlsx.NewFile()
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println(err)
		}
	}()
	sw, err := file.NewStreamWriter("Sheet1")
	if err != nil {
		fmt.Println(err)
		return
	}

	/* WRITE LEGEND */
	lrow := make([]interface{}, len(legend))
	for colID := 0; colID < len(legend); colID++ {
		lrow[colID] = legend[colID]
	}
	if err := sw.SetRow("A1", lrow); err != nil {
		fmt.Println(err)
	}
	if err := sw.Flush(); err != nil {
		fmt.Println(err)
		return
	}
	if err := file.SaveAs("Book1.xlsx"); err != nil {
		fmt.Println(err)
	}

	var sw1 *xlsx.StreamWriter = nil
	for i := 0; i < 1030001; i++ {

		if i%10000 == 0 {
			time.Sleep((1) * time.Second)
			fmt.Printf("STEP: %d/%d\n", i, 1030001)
		}

		row := make([]interface{}, len(legend))
		for colID := 0; colID < len(legend); colID++ {
			row[colID] = rand.Intn(640000)
		}
		cell, err := xlsx.CoordinatesToCellName(1, (i + 2))
		if err != nil {
			fmt.Println(err)
			break
		}
		if err := sw1.SetRow(cell, row); err != nil {
			fmt.Println(err)
			break
		}

	}

	if err := sw.Flush(); err != nil {
		fmt.Println(err)
		return
	}
	if err := file.SaveAs("Book1.xlsx"); err != nil {
		fmt.Println(err)
	}
}

package filedriver

import (
	"encoding/csv"
	"encoding/json"
	"go-scrapper/config"
	"go-scrapper/utils"
	"os"
)

var log = config.LOG

/*
0-2 - normal
3 - ubnormal
*/
type Source struct {
	Host              string   `json:"host"`
	UseFile           bool     `json:"use_file"`
	WriteAll          bool     `json:"write_all"`
	File              string   `json:"file"`
	URIs              []string `json:"uris"`
	TargetText        []string `json:"target_text"`
	ExcludeTargetText []string `json:"exlude_target_text"`
	TargetBlock       string   `json:"target_block"`
}

func ReadSource() (source Source, err error) {
	bbuf, err := os.ReadFile(config.CONF.Source.File)
	if err != nil {
		log.Error(err)
		return
	}
	if err := json.Unmarshal(bbuf, &source); err != nil {
		log.Error(err)
		return Source{}, err
	}
	if source.UseFile {
		source.URIs = readSourceCSV(source.File)
	}
	return
}

func readSourceCSV(path string) []string {
	file, err := os.Open(path)
	if err != nil {
		log.Fatalf("Error reading file [%s]. Err: %+v\n",
			path, err)
		return []string{}
	}
	defer file.Close()
	reader := csv.NewReader(file)
	data, err := reader.ReadAll()
	links := []string{}
	if err == nil {
		for pos, rows := range data {
			if pos > 0 {
				links = append(links, rows...)
			}
		}
	}
	return links
}

var userAgent []string

func init() {
	if config.CONF.ANB.UseUA {
		userAgent = readUserAgent()
	}
}

func readUserAgent() []string {
	bbuf, err := os.ReadFile(config.CONF.Source.File)
	if err != nil {
		log.Error(err)
		return []string{}
	}
	ua := []string{}
	if err := json.Unmarshal(bbuf, &ua); err != nil {
		log.Error(err)
		return []string{}
	}
	return ua
}

func RandomUserAgent() string {
	if len(userAgent) > 0 {
		return userAgent[utils.RandomSleep(len(userAgent)-1)]
	}
	return ""
}

package parser

import (
	"fmt"
	"go-scrapper/config"
	filereader "go-scrapper/file_driver"
	"go-scrapper/utils"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

var log = config.LOG

type Player struct {
	Name string
	// Company []Company
	OpeningOdds float64
	ClosingOdds float64
	BDate       *time.Time
}

type Company struct {
	Player1 Player
	Player2 Player
	Name    string
}

type Data struct {
	Excluded   bool
	Match      string
	Round      string
	Covering   string
	MatchName  string
	MatchPrize string
	MatchSex   string
	Score      string
	RoundScore string
	Date       *time.Time
	Player1    string
	Player1Uid string
	Player1BD  *time.Time
	Player2    string
	Player2Uid string
	Player1Url string
	Player2BD  *time.Time
	Company    []Company
}

func GetData() (data []Data) {
	// read source from config
	source, _ := filereader.ReadSource()
	limit := config.CONF.Output.FileLimit
	if limit == 0 {
		limit = 1000
	}
	// loop uri list
	// data = make(map[string]Data)
	start := 0

	body := [][]string{}
	for pos, uri := range source.URIs {
		// build link
		// URINormalize - check if uri start fron /
		link := fmt.Sprintf("%s%s", source.Host, utils.URINormalize(uri))
		// request the HTML page.
		client := &http.Client{}
		req, err := http.NewRequest("GET", link, nil)
		if err != nil {
			fmt.Println(link)
			fmt.Println(err)
		}
		// random UserAgent from list
		if config.CONF.ANB.UseUA {
			req.Header.Set("User-Agent", filereader.RandomUserAgent())
		}
		// make request
		res, err := client.Do(req)
		if err != nil {
			fmt.Println(err)
			break
		}
		defer res.Body.Close()
		// break if page not loaded
		if res.StatusCode != 200 {
			fmt.Println("status code error: %d %s", res.StatusCode, res.Status)
			break
		}
		// load the HTML document
		doc, err := goquery.NewDocumentFromReader(res.Body)
		if err != nil {
			fmt.Println(err)
			break
		}
		result := Data{}
		// find global search element
		parent := doc.Find(source.TargetBlock).Closest("div.content")
		playersBoard := doc.Find("div#center").Find("table.result.gDetail")
		p1Name := playersBoard.Find("th.plName").Eq(0).Find("a").Text()
		p1link := strings.Split(playersBoard.Find("th.plName").Eq(0).Find("a").AttrOr("href", "-"), "/")
		p1Uid := p1link[len(p1link)-1]
		p2Name := playersBoard.Find("th.plName").Eq(1).Find("a").Text()
		p2link := strings.Split(playersBoard.Find("th.plName").Eq(1).Find("a").AttrOr("href", "-"), "/")
		p2Uid := p2link[len(p2link)-1]
		p1BDate := utils.BDateParse(playersBoard.Find("tbody tr.two").Eq(0).Find("td").Eq(0).Text())
		p2BDate := utils.BDateParse(playersBoard.Find("tbody tr.two").Eq(0).Find("td").Eq(1).Text())

		gScore := playersBoard.Find("td.gScore")
		scoreData, _ := gScore.Html()
		score, roundScore, ok := utils.ScoreProcessor(scoreData)
		if !ok {
			score = strings.Split(gScore.Text(), "(")[0]
			roundScore = gScore.Find("span").Text()
		}

		matchDate := utils.MDateParse(doc.Find("div#center").Find("div.box.boxBasic.lGray").Eq(0).Find("span.upper").Text())
		round, covering := utils.MakeRoundAndCovering(doc.Find("div#center").Find("div.box.boxBasic.lGray").Eq(0).Text())
		uri, _ := doc.Find("div#center").Find("div.box.boxBasic.lGray").Eq(0).Find("a").Attr("href")
		name, prize, sex := GetChampData(uri, source, link)
		// return
		// loop on tables by selector (if more than 1)
		parent.Find(source.TargetBlock).Each(func(i int, s *goquery.Selection) {
			// match name
			result.Match = doc.Find("#center").Find(".box.boxBasic.lGray").Eq(0).Find("a").Text()
			player1 := Player{
				Name:  p1Name,
				BDate: p1BDate,
			}
			player2 := Player{
				Name:  p2Name,
				BDate: p2BDate,
			}
			// find player names
			s.Find("tr.head").Each(func(i int, tr *goquery.Selection) {
				player1.Name = tr.Find("td.k1").Text()
				if player1.Name == p1Name {
					player1.BDate = p1BDate
				}
				if player1.Name == p2Name {
					player1.BDate = p2BDate
				}
				player2.Name = tr.Find("td.k2").Text()
				if player2.Name == p2Name {
					player2.BDate = p2BDate
				}
				if player2.Name == p1Name {
					player2.BDate = p1BDate
				}
			})
			companyArr := []Company{}
			for _, val := range source.TargetText {
				companyArr = append(companyArr, Company{
					Name: val,
				})
			}
			spotExlude := false
			// select all result match rows
			s.Find("td.first.tl").Each(func(i int, td *goquery.Selection) {
				// find ONLY rows from config by name match
				if utils.StringInArray(td.Find("a").Text(), source.ExcludeTargetText) {
					spotExlude = true
				}
				if utils.StringInArray(td.Find("a").Text(), source.TargetText) {
					// memory company name
					// + remove unwanted spaces
					src := utils.StringNormalize(td.Find("a").Text())
					// select result column data
					k1 := td.Closest("tr").Find("td.k1").Find("div.odds-in")
					k2 := td.Closest("tr").Find("td.k2").Find("div.odds-in")

					// get result text for player1 && player2
					k1openOdds := k1.Text()
					k1closeOdds := k1.Text()
					k2openOdds := k2.Text()
					k2closeOdds := k2.Text()

					// check if result column has nestet table
					if k1.HasClass("oup") || k1.HasClass("odown") {
						max := k1.Find("tr").Length()
						k1.Find("tr").Each(func(i int, innerTrs *goquery.Selection) {
							// Closing Odds ALWAYS in top of nested table
							if i == 0 {
								k1openOdds = innerTrs.Find("td").Eq(1).Text()
							}
							// Opening Odds ALWAYS in bottom of nested table
							if i == max-1 {
								k1closeOdds = innerTrs.Find("td").Eq(1).Text()
							}
						})
					}
					// check if result column has nestet table
					if k2.HasClass("odown") || k2.HasClass("oup") {
						max := k2.Find("tr").Length()
						k2.Find("tr").Each(func(i int, innerTrs *goquery.Selection) {
							// Closing Odds ALWAYS in top of nested table
							if i == 0 {
								k2openOdds = innerTrs.Find("td").Eq(1).Text()
							}
							// Opening Odds ALWAYS in bottom of nested table
							if i == max-1 {
								k2closeOdds = innerTrs.Find("td").Eq(1).Text()
							}
						})
					}
					player1.OpeningOdds = utils.ToFloat64(k1openOdds)
					player1.ClosingOdds = utils.ToFloat64(k1closeOdds)

					player2.OpeningOdds = utils.ToFloat64(k2openOdds)
					player2.ClosingOdds = utils.ToFloat64(k2closeOdds)
					// build result
					companyArr[indexByName(src, companyArr)] = Company{
						Name:    src,
						Player1: player1,
						Player2: player2,
					}
				}
			})
			if !spotExlude {
				result.Player1 = player1.Name
				result.Player1BD = player1.BDate
				result.Player1Uid = p1Uid
				result.Player2 = player2.Name
				result.Player2BD = player2.BDate
				result.Player2Uid = p2Uid
				result.Company = companyArr
				result.Date = matchDate
				result.Round = round
				result.Covering = covering
				result.MatchName = name
				result.MatchPrize = prize
				result.MatchSex = sex
				result.Score = score
				result.RoundScore = roundScore
				// fmt.Printf("%+v\n", result)
			} else {
				result.Excluded = true
			}
		})
		fmt.Printf("HOST:[%s] PROCESSING: [%d/%d]\t", link, (pos + 1), len(source.URIs))

		if result.Player1 != "" && result.Player2 != "" {
			data = append(data, result)
			fmt.Printf("pool:[%d/%d]\n", len(data), limit)
			row := []string{}

			row = append(row, result.Match)
			row = append(row, utils.TimePrepare(result.Date, false))
			row = append(row, result.Round)
			row = append(row, result.Covering)
			row = append(row, result.MatchName)
			row = append(row, result.MatchPrize)
			row = append(row, result.MatchSex)
			row = append(row, result.Score)
			row = append(row, result.RoundScore)
			row = append(row, result.Player1)
			row = append(row, utils.TimePrepare(result.Player1BD, false))
			row = append(row, result.Player1Uid)
			row = append(row, result.Player2)
			row = append(row, utils.TimePrepare(result.Player2BD, false))
			row = append(row, result.Player2Uid)

			for _, odds := range result.Company {
				row = append(row, utils.PrepareFloat64(odds.Player1.OpeningOdds))
				row = append(row, utils.PrepareFloat64(odds.Player1.ClosingOdds))

				row = append(row, utils.PrepareFloat64(odds.Player2.OpeningOdds))
				row = append(row, utils.PrepareFloat64(odds.Player2.ClosingOdds))
			}
			// row = append(row, link)
			body = append(body, row)
			if (len(data) != 0 && (len(data)%limit) == 0) ||
				(pos+1) == len(source.URIs) {
				filereader.WriteCSV(body, fmt.Sprintf("%d-%d", start, pos))
				filereader.WriteXLSX(body, fmt.Sprintf("%d-%d", start, pos))
				fmt.Printf("Pool closed in (%d) - ", len(data))
				start = pos
				body = nil
				data = nil
				fmt.Printf("check empty (%d)\n", len(data))
			}
		} else {
			if result.Excluded {
				fmt.Printf("[%s] found EXCLUDE data, ignored\n", link)
			} else {
				fmt.Printf("[%s] is empty, ignored\n", link)
			}
		}
		if config.CONF.ANB.TO != 0 {
			time.Sleep(time.Duration(utils.RandomSleep(config.CONF.ANB.TO)) * time.Second)
		}
	}
	return
}

func indexByName(s string, ca []Company) int {
	for pos, val := range ca {
		if val.Name == s {
			return pos
		}
	}
	return 0
}

func GetChampData(uri string, source filereader.Source, parent string) (name, prize, sex string) {
	if uri == "" {
		fmt.Printf("Champ data not exist [%s]\n", parent)
		return
	}
	link := fmt.Sprintf("%s%s", source.Host, utils.URINormalize(uri))
	// request the HTML page.
	client := &http.Client{}
	req, err := http.NewRequest("GET", link, nil)
	if err != nil {
		fmt.Printf("Warning on [%s] ERR: %+v\n", link, err)
	}
	// random UserAgent from list
	if config.CONF.ANB.UseUA {
		req.Header.Set("User-Agent", filereader.RandomUserAgent())
	}
	// make request
	res, err := client.Do(req)
	if err != nil {
		fmt.Printf("Panic on [%+v] ERR: %+v\n", link, err)
		return
	}
	defer res.Body.Close()
	// break if page not loaded
	if res.StatusCode != 200 {
		fmt.Printf("status code error: %d %s", res.StatusCode, res.Status)
		return
	}
	// load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}

	name = doc.Find("div#center").Find("h1.bg").Text()
	dataBox := doc.Find("div#center").Find("div.box.boxBasic.lGray").Eq(0).Text()
	dataBox = strings.Replace(dataBox, "(", "", -1)
	dataBox = strings.Replace(dataBox, ")", "", -1)
	prize = utils.ReplaceNoDigits(dataBox)
	dataSplit := strings.Split(dataBox, ",")
	sex = utils.StringNormalize(dataSplit[len(dataSplit)-1])
	return
}

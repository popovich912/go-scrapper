package config

import (
	"encoding/json"
	"fmt"
	"go-scrapper/utils"
	"os"

	log "github.com/sirupsen/logrus"
)

type Config struct {
	Daemon struct {
		DevMode bool `json:"devmode"`
	} `json:"daemon"`
	Output struct {
		CSV  string `json:"csv"`
		XLSX string `json:"xlsx"`

		DateFull  bool `json:"file-date-full"`
		FileLimit int  `json:"file-limit"`
	} `json:"output"`
	Logs struct {
		OS string `json:"os"`
	} `json:"logs"`
	Source struct {
		File      string `json:"file"`
		UserAgent string `json:"user-agent"`
	}
	ANB struct {
		TO    int  `json:"timeout"`
		UseUA bool `json:"use-ua"`
	} `json:"anti-ban"`
}

var (
	CONF Config
	LOG  *log.Logger = log.New()
)

func init() {
	bbuf, err := os.ReadFile("../assets/config.conf")
	if err != nil {
		fmt.Printf("[ERROR READ LOG]:%+v\n", err)
		os.Exit(1)
		return
	}

	// LOG = log.New()
	LOG.SetOutput(os.Stderr)
	LOG.SetLevel(log.DebugLevel)
	LOG.Hooks = make(log.LevelHooks)
	LOG.ReportCaller = true
	LOG.SetFormatter(&log.JSONFormatter{
		FieldMap: log.FieldMap{
			log.FieldKeyTime:  "@timestamp",
			log.FieldKeyLevel: "@level",
			log.FieldKeyMsg:   "@message",
			log.FieldKeyFunc:  "@caller",
		},
		PrettyPrint:       true,
		DisableHTMLEscape: true,
		TimestampFormat:   "2006.01.02 15:04:05",
	})
	// LOG = &log.Logger{
	// 	Out:   os.Stderr,
	// 	Level: log.DebugLevel,
	// 	Hooks: make(log.LevelHooks),
	// 	// Formatter: &log.JSONFormatter{},
	// }
	if err := json.Unmarshal(bbuf, &CONF); err != nil {
		fmt.Printf("[ERROR PARSE LOG]:%+v\n", err)
		os.Exit(1)
		return
	}

	utils.CheckPath(CONF.Output.CSV)
	utils.CheckPath(CONF.Output.XLSX)
	utils.CheckPath(CONF.Logs.OS)

	// Log as JSON instead of the default ASCII formatter.
	// log.SetFormatter()

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	// log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	// log.SetLevel(log.WarnLevel)
}

package main

import (
	"fmt"
	"go-scrapper/parser"

	"github.com/pbnjay/memory"
)

// var log = config.LOG

func main() {

	free := memory.FreeMemory()
	kb := free / 1024
	mb := kb / 1024
	gb := mb / 1024

	fmt.Printf("Free system memory: %d Gb %d Mb \n", gb, mb%1024)

	// return
	parser.GetData()
	// body := [][]string{}
	// for _, data := range result {
	// 	row := []string{}

	// 	row = append(row, data.Match)
	// 	row = append(row, utils.TimePrepare(data.Date, false))
	// 	row = append(row, data.Player1)
	// 	row = append(row, utils.TimePrepare(data.Player1BD, false))
	// 	row = append(row, data.Player2)
	// 	row = append(row, utils.TimePrepare(data.Player2BD, false))

	// 	for _, odds := range data.Company {
	// 		row = append(row, utils.PrepareFloat64(odds.Player1.OpeningOdds))
	// 		row = append(row, utils.PrepareFloat64(odds.Player1.ClosingOdds))

	// 		row = append(row, utils.PrepareFloat64(odds.Player2.OpeningOdds))
	// 		row = append(row, utils.PrepareFloat64(odds.Player2.ClosingOdds))
	// 	}
	// 	body = append(body, row)
	// }
	// filedriver.WriteCSV(body)
	// filedriver.WriteXLSX(body)
	// filedriver.WriteXLSXTest()
}

/*
[16274/664129]
[https://www.tennisexplorer.com/match-detail/?id=739392] is empty, ignored
{
  "@caller": "go-scrapper/parser.GetData",
  "@level": "fatal",
  "@message": "Get \"https://www.tennisexplorer.com/match-detail/?id=739396\": dial tcp 188.92.41.176:443: connectex: No connection could be made because the target machine actively refused it.",
  "@timestamp": "2023.07.14 10:06:05",
  "file": "C:/Users/PinQueen/work/golang/tennis-scrapper/src/parser/parser.go:64"
}
exit status 1
*/

# Website scrapper
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Goes through the list of sites and collects information on specific selector

## BIN
- compiled *.exe for Windows x64

## SRC
- cource code

## ASSETS
- config file
- User-Agent list

## OUT
- result output files, sort by type

## IN
- source *.csv directory

# config.conf
>
    {
	"daemon":{
	    // logging level
		"devmode": false 
	},
	"output":{ 
	    // path for result files
		"csv": "../out/csv", 
		"xlsx": "../out/xls",
        // filename date format
		"file-date-full": false (dd.mm.YYYY) | true (dd.mm.YYYY_HH:MM)
	},
	"logs":{
		"os":"os_errors.log"
	},
	"source":{
	    // source json path
		"file":"../assets/source.json",
		// user-agent json path
		"user-agent": "../assets/user-agents.json"
	},
	"anti-ban": {
	    // max timeout
		"timeout": 5,
		// use useragent
		"use-ua": false
	}
    }

# source.json
    {
    // source host
		"host":"https://www.tennisexplorer.com",
		// get source from file :?:
		"use_file":true,
		// path to source file (ONLY if "use_file":true)
		"file":"../in/links.csv",
		// source uri list
		"uris":[
			"/match-detail/?id=2288931", 
			"/match-detail/?id=2288229", 
			"/match-detail/?id=2288933", 
			"/match-detail/?id=830631" 
		],
		// target names
		"target_text":[
			"bet365",
			"Pinnacle"
		],
		// exclude target names (ignore link if match)
		"exlude_target_text":[
			"Pinnacle"
		],
		// target block closest selector
		"target_block":"div[id*=\"oddsMenu-1-data\"]"
    }
